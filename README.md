# Instruções

Desenvolver um script utilizando, preferencialmente, NodeJS que lê os dados das tabelas na pasta do backup ( `backup/backup-data.sql` ) e insere nas tabelas do banco referenciado no arquivo ```database/db-tables.sql```.

Os dados do backup devem ser manipulados e tratados de maneira que seja possível inseri-los nas tabelas do banco referenciado no arquivo `database/db-tables.sql`

Descrever o processo de implementação e as etapas que são necessárias para a resolução do problema, caso não conseguir implementar, escrever o passo a passo do que se deve fazer comentado no arquivo `comentarios.md`

Caso tenha dúvida com alguma tabela de produção ou atributo, verificar o comentário do mesmo no arquivo `db-tables.sql`

Pode ser utilizada qualquer linguagem para resolver o problema, se for possível use NodeJS.

As queries não precisam ser otimizadas.

Entregar projeto final com um dump da base de `produção` em um fork do repositório ou em zip por e-mail

#### Recomendações:
* Caso for implementar em NodeJS - Para a conexão com o mysql usar: https://github.com/sidorares/node-mysql2

* Evitar linkar um dado com o outro no script `(fazer isso direto nas queries)`
* Preencher as tabelas de maneira assíncrona em processos `(funções)` separados
* Interligar um dado de uma tabela com a outra através do codigo de importação (fazer um `SELECT` caso seja necessário receber os dados de uma tabela de produção já inserida)

## Será avaliado:
* Legibilidade do código
* Resultado final dos dados na database de produção
* Queries para receber os dados do backup
* Tratamento dos dados do backup recebidos nas queries do backup
